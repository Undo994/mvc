<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="java.util.*, it.undo.model.entities.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lista Clienti</title>
</head>
<body>
	<%--Il CAST serve perchè bisogna confermare "l' oggetto" lista clienti--%>
	<%
	List<Cliente> listaClienti = (List<Cliente>) request.getAttribute("listaClienti");
	for (Cliente c : listaClienti) {
		out.print(c.getId() + "  " + c.getNome() + "  " + c.getCognome() + "<br>");
	}
	%>
</body>
</html>