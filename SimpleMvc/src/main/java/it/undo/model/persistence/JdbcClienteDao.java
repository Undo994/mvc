package it.undo.model.persistence;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.undo.model.entities.Cliente;

public class JdbcClienteDao implements ClienteDao {

	private static final String SELECT_ALL_CLIENTS = "select id, nome, cognome from cliente";
	private static final String INSERT_SAVE_CLIENTS = "insert into cliente(id, nome, cognome) values(?,?,?);";
	private static final String SELECT_ALL_CLIENTS_BY_LIKE = "select id, nome, cognome from cliente where cognome like ?";

	@Override
	public List<Cliente> getAll() throws SQLException {
		/**
		 * TRY WITH RESOURCES
		 * 
		 * Possiamo istanziare oggetti di una classe che implementano "AutoCloseable",
		 * una volta istanziato l'oggetto verrà chiuso automaticamente.
		 */
		try (Connection con = JdbcUtils.createConnection();
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery(SELECT_ALL_CLIENTS)) {

			List<Cliente> result = clientiFromResultset(rs);
			return result;

		}
	}

	@Override
	public Cliente save(Cliente c) throws SQLException {
		try (Connection con = JdbcUtils.createConnection();
				PreparedStatement ps = con.prepareStatement(INSERT_SAVE_CLIENTS);) {
			ps.setLong(1, c.getId());
			ps.setString(2, c.getNome());
			ps.setString(3, c.getCognome());
			ps.executeUpdate();
		}
		return c;

	}

	@Override
	public List<Cliente> getByCognomeLike(String part) throws SQLException {
		try (Connection con = JdbcUtils.createConnection();
				PreparedStatement ps = con.prepareStatement(SELECT_ALL_CLIENTS_BY_LIKE);) {

			ps.setString(1, "%s" + part + "%s");

			try (ResultSet rs = ps.executeQuery()) {

				List<Cliente> result = clientiFromResultset(rs);

				return result;

			}
		}
	}
	
	/** Metodo per iterare la lista di clienti su DB */
	private List<Cliente> clientiFromResultset(ResultSet rs)
			throws SQLException { 

		List<Cliente> listaClienti = new ArrayList<>();

		while (rs.next()) {
			listaClienti.add(new Cliente(rs.getLong("id"), rs.getString("nome"), rs.getString("cognome")));
		}
		return listaClienti;

	}

}
