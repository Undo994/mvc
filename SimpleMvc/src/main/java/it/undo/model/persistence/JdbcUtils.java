package it.undo.model.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcUtils {

	public static final String DRIVER_DB = "org.postgresql.Driver";
	public static final String JDBC_NAME = "postgresql";
	public static final String HOST = "localhost";
	public static final int PORT = 5432;
	public static final String DB_URL = "AZIENDA";
	public static final String SCHEMA = "public";
	public static final String USERNAME = "postgres";
	public static final String PASSWORD = "aalmstgf";

	public static final String JDBC_URL = String.format("jdbc:%s://%s:%d/%s?currentSchema=%s&user=%s&password=%s", 
			JDBC_NAME, HOST, PORT, DB_URL, SCHEMA, USERNAME, PASSWORD);

	public static Connection createConnection() throws SQLException {
		return DriverManager.getConnection(JDBC_URL);
	}

}
