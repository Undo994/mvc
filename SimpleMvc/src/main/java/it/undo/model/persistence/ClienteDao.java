package it.undo.model.persistence;

import java.sql.SQLException;
import java.util.List;

import it.undo.model.entities.Cliente;

public interface ClienteDao {
	
	List<Cliente> getAll() throws SQLException;
	Cliente save(Cliente c) throws SQLException;
	List<Cliente> getByCognomeLike(String part) throws SQLException;
}
