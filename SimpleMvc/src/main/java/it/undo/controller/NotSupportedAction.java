package it.undo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NotSupportedAction implements Action {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		return "NotSupported.jsp";
	}

}
