package it.undo.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.undo.model.entities.Cliente;
import it.undo.model.persistence.ClienteDao;

public class AggiungiClienteAction implements Action{
	
	private ClienteDao clienteDao;
	

	public AggiungiClienteAction(ClienteDao addCliente) {
		this.clienteDao = addCliente;
	}



	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws SQLException {
		String id = request.getParameter("id");
		String nome = request.getParameter("nome");
		String cognome = request.getParameter("cognome");
		
		Cliente cli = clienteDao.save(new Cliente(Integer.parseInt(id), nome, cognome));
		
		request.setAttribute("cliente", cli);
		
		return "confermaInserimento.jsp";
	}

}
