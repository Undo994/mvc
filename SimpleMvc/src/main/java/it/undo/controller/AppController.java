package it.undo.controller;

/**
 * Nelle app MVC il Controller è una Servlet che riceve la richiesta dall'utente,
 *  avvia l'azione corrispondente, ottine il risultato e la fa visializzare da una pagina JSP
 */

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.undo.model.persistence.JdbcClienteDao;

/**
 * l'"*" in informatica è una WildCard, ovvero un carattere speciale che non
 * rappresenta sé stesso e può essere interpretato come uno o più altri
 * caratteri oppure una stringa vuota. Qualsiasi richiesta che verrà fatta
 * dall'utente verrà passata alla Servlet.
 */

@WebServlet("*.do")
public class AppController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * "Costruttore statico", pezzo di codice che verrà lanciato appena la VM Leggerà la classe.
	 */
	static {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String actionString = request.getServletPath();  /** Capisce con quale URL è stato chiamato ES. "/listaClienti.do" */
																/**
		 														* "lastIndexOf()" Restituisce l'indice all'interno di questa stringa
		 														* dell'ultima occorrenza della sottostringa specificata. Si considera che
		 														* l'ultima occorrenza della stringa vuota "" si verifichi al valore dell'indice
		 														* this.length().
		 														*/
		
		int pos = actionString.lastIndexOf("/"); 				/** voglio l'ultimo "/" che è presente nella stringa */
		
		actionString = actionString.substring(pos + 1,
		actionString.length() - 3); 								/** "pos + 1" prendi la posizione della stringa e
																  * riprendi la sua lunghezza tranne gli ultimi 3
																  * caratteri(-3)
		 														  */
		Action action = null;
		switch(actionString) { /** qui capisce con quale stringa viene "Azionata" dal cliente*/
		case "listaClienti": 
			action = new ListaClienteAction(new JdbcClienteDao()); /** se azionato "Lista clienti", istanzierà la classe */
			break;
		case "aggiungiCliente":
			action = new AggiungiClienteAction(new JdbcClienteDao());
			break;	
			default: 
				action = new NotSupportedAction();
				break;	
		}
		String viewName = null;
		try {
			viewName = action.execute(request, response); /**La variabile "viewName" ritornerà l'esecuzione delle "execute" ovverò la richiesta del cliente in formato pagina JSP */
			RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/" + viewName);
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
