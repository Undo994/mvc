package it.undo.controller;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.undo.model.entities.Cliente;
import it.undo.model.persistence.ClienteDao;

public class ListaClienteAction implements Action {
	
	/**
	 * DEPENDECY INVERSION
	 */

	private ClienteDao clienteDao;
	
	public ListaClienteAction(ClienteDao clienteDao) {
		this.clienteDao = clienteDao;
	}
	
	/**
	 * Il controller istanzierà la classe che viene richiamata dal cliente con il metodo "execute".
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws SQLException {
		
		List<Cliente> rs = clienteDao.getAll();    
		request.setAttribute("listaClienti", rs);
		return "listaClienti.jsp";
	}

}
